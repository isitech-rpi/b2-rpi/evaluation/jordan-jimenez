#pragma once
#ifndef EQUIPMOTO_H
#define EQUIPMOTO_H

#include <string>
#include "Personne.h"
class EquipeMoto
{
private:
    std::string nom;
    Personne* manager;
    Personne* lesPilotes[3];

public:
    EquipeMoto(const std::string& nomEquipe, const std::string& nomManager);

    ~EquipeMoto();

    std::string getNom() const;
    void setNom(const std::string& nom);

    Personne* getManager() const;

    Personne** getPilotes();
    void addPilote(unsigned int rang, Personne* pilote);
};

#endif

