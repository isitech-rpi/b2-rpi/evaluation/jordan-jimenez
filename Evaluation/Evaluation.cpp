#include <iostream>
#include "Personne.h"
#include "EquipeMoto.h"

void AffichePersonne(const Personne& p) {
    std::cout << "Personne(" << &p << "): " << p.getNom() << std::endl;
}

Personne* CreerPersonne() {
    std::string nom;
    std::cout << "nom de la personne: ";
    std::cin >> nom;
    return new Personne(nom);
}

void AfficheEquipeMoto(EquipeMoto* equipe) {
    std::cout << "Nom equipe : " << equipe->getNom() << std::endl;
    std::cout << "Nom manager : " << equipe->getManager()->getNom() << std::endl;
    Personne** pilotes = equipe->getPilotes();
    for (int i = 0; i < 3; ++i) {
        if (pilotes[i] != nullptr) {
            std::cout << "Pilote " << i + 1 << ": " << pilotes[i]->getNom() << std::endl;
        }
        else {
            std::cout << "Pas de pilote " << i + 1 << std::endl;
        }
    }
}

int main() {
    Personne pilote_1("Fabio");

    AffichePersonne(pilote_1);

    Personne* pilote_2 = CreerPersonne();

    AffichePersonne(*pilote_2);



    EquipeMoto* equipe_1 = new EquipeMoto("YMF", "Jarvis");

    equipe_1->addPilote(0, new Personne("Fabio")); 
    equipe_1->addPilote(1, pilote_2);

    AfficheEquipeMoto(equipe_1);





    Personne* pil_pramac_0 = new Personne("Eliot");
    Personne* pil_pramac_2 = new Personne("Jules");

    EquipeMoto pramac = *equipe_1;

    pramac.setNom("Pramac");

    pramac.getManager()->setNom("Campignoti");

    pramac.addPilote(0, pil_pramac_0);
    pramac.addPilote(2, pil_pramac_2);

    std::cout << "\nEquipe YMF:" << std::endl;
    AfficheEquipeMoto(equipe_1);

    std::cout << "\nEquipe Pramac:" << std::endl;
    AfficheEquipeMoto(&pramac);

    delete equipe_1;
    delete pil_pramac_0;
    delete pil_pramac_2;

    return 0;
}