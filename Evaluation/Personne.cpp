#include "Personne.h"

Personne::Personne(const std::string& nom) : nom(nom) {}

std::string Personne::getNom() const {
    return nom;
}

void Personne::setNom(const std::string& nom) {
    this->nom = nom;
}
