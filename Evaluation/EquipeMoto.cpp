#include "EquipeMoto.h"

EquipeMoto::EquipeMoto(const std::string& nomEquipe, const std::string& nomManager)
    : nom(nomEquipe), manager(new Personne(nomManager)) {
    for (int i = 0; i < 3; ++i) {
        lesPilotes[i] = nullptr;
    }
}
EquipeMoto::~EquipeMoto() {
    delete manager;
    for (int i = 0; i < 3; ++i) {
        if (lesPilotes[i] != nullptr) {
            delete lesPilotes[i];
        }
    }
}

std::string EquipeMoto::getNom() const {
    return nom;
}

void EquipeMoto::setNom(const std::string& nom) {
    this->nom = nom;
}

Personne* EquipeMoto::getManager() const {
    return manager;
}


Personne** EquipeMoto::getPilotes() {
    return lesPilotes;
}

void EquipeMoto::addPilote(unsigned int rang, Personne* pilote) {
    if (rang < 3) {
        lesPilotes[rang] = pilote;
    }
}